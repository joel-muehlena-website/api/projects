import { Router } from "express";
import { createProject } from "../../common/v1/createProject";
import { deleteProject } from "../../common/v1/deleteProject";
import { editProject } from "../../common/v1/editProject";
import { getProjectById, getProjects } from "../../common/v1/getProjects";

export const projectV1Router = Router();

projectV1Router.get("/project", getProjects);
projectV1Router.get("/project/:id", getProjectById);
projectV1Router.post("/project", createProject);
projectV1Router.put("/project/:id", editProject);
projectV1Router.delete("/project/:id", deleteProject);
