import { Request, Response } from "express";
import { NextFunction } from "express-serve-static-core";

import { Project } from "../../models/v1/Project";
import { Project as IProject } from "../../models/v1/Project.interface";

export const getProjects = async (
  _: Request,
  res: Response,
  next: NextFunction
) => {
  const projects: Array<IProject> = await Project.find();

  if (projects.length <= 0) {
    res.status(404).json({ code: 404, msg: "No project entry found" });
    return next();
  }

  res.json({ code: 200, msg: "Success", data: projects });

  return next();
};

export const getProjectById = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params.id;
  const project: IProject = await Project.findById(id);

  if (!project) {
    res
      .status(404)
      .json({ code: 404, msg: `No project entry with the id: ${id} found` });
    return next();
  }

  res.json({ code: 200, msg: "Success", data: project });
  return next();
};
