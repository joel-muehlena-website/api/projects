import { Request, Response } from "express";
import { NextFunction } from "express-serve-static-core";

import { Project } from "../../models/v1/Project";
import { Project as IProject } from "../../models/v1/Project.interface";

export const editProject = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params.id;

  const project: IProject = await Project.findById(id);

  if (!project) {
    res.status(404).json({
      code: 404,
      msg: `No project entry with thw id ${id} found. Could not be edited.`,
    });

    return next();
  }

  const {
    name,
    shortDescription,
    description,
    showcaseImage,
    images,
    from,
    to,
    current,
    urls,
    skills,
  } = req.body;

  const updatedProject: any = {};
  if (name) updatedProject.name = name;
  if (shortDescription) updatedProject.shortDescription = shortDescription;
  if (description) updatedProject.description = description;
  if (showcaseImage) updatedProject.showcaseImage = showcaseImage;
  if (images) updatedProject.images = images;
  if (from) updatedProject.from = from;
  if (to) updatedProject.to = to;
  if (current === true || current === false) updatedProject.current = current;
  if (urls) updatedProject.urls = urls;
  if (skills) updatedProject.skills = skills;

  updatedProject.updatedAt = new Date();

  const updatedProjectData = await Project.findByIdAndUpdate(
    id,
    { $set: updatedProject },
    { new: true }
  );

  if (updatedProjectData) {
    res
      .status(200)
      .json({ msg: 200, message: "Success", data: updatedProjectData });
    return next();
  } else {
    res
      .status(500)
      .json({ code: 500, msg: "Failed to update the project entry" });
    return next();
  }
};
