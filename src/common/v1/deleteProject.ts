import { Request, Response } from "express";
import { NextFunction } from "express-serve-static-core";

import { Project } from "../../models/v1/Project";
import { Project as IProject } from "../../models/v1/Project.interface";

export const deleteProject = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const id = req.params.id;

  const project: IProject = await Project.findById(id);

  if (!project) {
    res
      .status(404)
      .json({ code: 404, msg: `No project entry with the id: ${id} found` });
    return next();
  }

  const delData: IProject = await Project.findByIdAndDelete(id);

  if (delData) {
    res.json({ code: 200, msg: "Success", data: delData });

    return next();
  } else {
    res.status(500).json({ code: 500, msg: "Failed to delete project entry" });

    return next();
  }
};
