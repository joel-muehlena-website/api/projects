import { Request, Response } from "express";
import { NextFunction } from "express-serve-static-core";

import { Project } from "../../models/v1/Project";
import { Project as IProject } from "../../models/v1/Project.interface";

export const createProject = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const {
    name,
    shortDescription,
    description,
    showcaseImage,
    images,
    from,
    to,
    current,
    urls,
    skills,
  } = req.body;

  const date = new Date();

  const newProjectEntry: IProject = {
    name,
    shortDescription,
    description,
    showcaseImage,
    images,
    from,
    to,
    current,
    urls,
    skills,
    createdAt: date,
    updatedAt: date,
  };

  const saveProjectEntry = await new Project(newProjectEntry).save();

  if (saveProjectEntry) {
    res
      .status(200)
      .json({ msg: 200, message: "Success", data: saveProjectEntry });
    return next();
  } else {
    res
      .status(500)
      .json({ code: 500, msg: "Failed to create the project entry" });
    return next();
  }
};
