export interface Project {
  name: string;
  shortDescription: string;
  description: string;
  showcaseImage: string;
  images: Array<string>;
  from: number;
  to: number;
  current: boolean;
  urls: Array<string>;
  skills: Array<string>;
  createdAt: Date;
  updatedAt: Date;
}
