import { Schema, model } from "mongoose";

const ProjectsSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  shortDescription: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  showcaseImage: {
    type: String,
    required: true,
  },
  images: {
    type: Array,
    required: false,
  },
  from: {
    type: Number,
    required: true,
  },
  to: {
    type: Number,
    required: false,
  },
  current: {
    type: Boolean,
    default: false,
  },
  urls: {
    type: Array,
    required: false,
  },
  skills: {
    type: Array,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

export const Project = model("projects", ProjectsSchema);
